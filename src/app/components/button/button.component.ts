import { Component, HostBinding, Input } from '@angular/core';
import { convertToBoolProperty } from '../../helpers';

@Component({
  selector: 'button[appButton],a[appButton],input[type="button"][appButton],input[type="submit"][appButton]',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {

  @HostBinding('class.btn-primary')
  private primaryValue: boolean;
  @Input() set primary(value) {
    this.primaryValue = convertToBoolProperty(value);
  }

  @HostBinding('class.btn-small')
  private smallValue: boolean;
  @Input() set small(value) {
    this.smallValue = convertToBoolProperty(value);
  }

}
