import { Component, HostBinding, Input } from '@angular/core';
import { convertToBoolProperty } from '../../helpers';

@Component({
  selector: 'app-tooltip',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent {

  @HostBinding('class.on-top')
  private onTopValue: boolean;
  @Input() set onTop(value) {
    this.onTopValue = convertToBoolProperty(value);
  }

  @HostBinding('class.on-bottom')
  private onBottomValue: boolean;
  @Input() set onBottom(value) {
    this.onBottomValue = convertToBoolProperty(value);
  }

  @HostBinding('class.on-left')
  private onLeftValue: boolean;
  @Input() set onLeft(value) {
    this.onLeftValue = convertToBoolProperty(value);
  }

  @HostBinding('class.on-right')
  private onRightValue: boolean;
  @Input() set onRight(value) {
    this.onRightValue = convertToBoolProperty(value);
  }

}
